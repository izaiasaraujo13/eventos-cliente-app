const tintColor = '#009DF8';

export default {
  tintColor,

  primary: '#3f4d5a',
  white: '#FFF',
  light: '#F2FAFF',
  dark: '#456782',

  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
